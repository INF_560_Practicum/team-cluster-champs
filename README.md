# Cluster Champs repository
The repo has one source directory "src/" which contains three separate components, one for each component of our project: classifier, heatmap dashboard, recommender system and dashboard.
```
+-- src
|   +-- ML_models
|   +-- heatmaps
|   +-- Recommendation_system

```
### ML_models
This folder contains mostly jupyter scripts that are implementations of various hybrid model for labeling clustered data, creation of our test set and for predictive models.
```
+-- ML_models
|	+-- notebooks
	|	+-- Hybrid_model
		|   +-- sample_spark_code.ipynb
		|   +-- simple_classifier.ipynb
	|	+-- Test Dataset
		|   +-- TestData_Kiana.ipynb
		|   +-- Group_travelers_data.ipynb
	|	+-- Predictive_model
		|   +-- PredictiveSolo.ipynb
		|   +-- PredictiveGroup.ipynb
|	+-- data
	|   +-- matching.ipynb
	|   +-- allData.csv
```

### Heatmap Dashboard
```
+-- heatmaps
|   +-- heatmaps-web (React app for front end)
|   +-- heatmaps-api (Backend api to supply data)
```
### Recommendation system
```
+-- RS
|   +--NLP _Accuracy _Mesure on Data Set (1).ipynb
|   +-- Recommendation system generator (2).ipynb
|   +-- nlp_excel.xlsx
|   +-- Review.xlsx
```
