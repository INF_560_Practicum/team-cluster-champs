# Deploying Dasbhoards

## General Setup
```
git clone https://bitbucket.org/INF_560_Practicum/team-cluster-champs/src
cd src
cd dashboards
```

## Express API
```
cd heatmaps-api
npm install
DEBUG=heatmaps:* npm start
```

Test the express server by going to localhost:3000/data.

## React Web Server
```
yarn start
```

This will popup a window with the landing page for both dashboards.
