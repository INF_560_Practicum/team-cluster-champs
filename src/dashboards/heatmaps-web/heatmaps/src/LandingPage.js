import React from 'react';
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link,
    useRouteMatch
  } from "react-router-dom";
import HeatmapDashboard from './components/HeatmapDashboard';
import Button from '@material-ui/core/Button';
import { makeStyles } from '@material-ui/core/styles';
import Background from '../src/images/rio.png';

import './App.css';

const useStyles = makeStyles((theme) => ({
  root: {
    '& > *': {
      margin: theme.spacing(1),
    },
  },
}));



function ContainedButtons() {
    let match = useRouteMatch();
  const classes = useStyles();

  return (
    <div>
    <div className={classes.root}>
      <Button variant="contained">
      <Link to="/">Home</Link>
      </Button>
      <Button variant="contained">
        <Link to="/heatmaps">Heatmap Dashboard</Link>
      </Button>
      <Button variant="contained">
      <Link to="/rs">Rec System</Link>
      </Button>
    </div>
    </div>

  );
}

  function LandingPage(){
      return(
          <div className="landing-page" style={{backgroundImage: "url(" + Background + ")", backgroundSize : "cover"}}>
              <br/> <br/> <br/> <br/> <br/>
              
              <div className="main-title">Cluster Champs Dashboard</div>
              <ContainedButtons/>
          </div>
      )
  }

  export default LandingPage;