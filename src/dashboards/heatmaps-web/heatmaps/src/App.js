import React from 'react';
import logo from './logo.svg';
import './App.css';
import HeatmapDashboard from './components/HeatmapDashboard';
import LandingPage from './LandingPage';
import RecSys from './components/RecSys';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
  useRouteMatch,
} from "react-router-dom";


function App() {
  return (
    <div className="App">
    <Router>
        <Switch>
        <Route exact path="/" component={LandingPage} />
        <Route exact path="/heatmaps" component={HeatmapDashboard} />
        <Route exact path="/rs" component={RecSys} />
      </Switch>
    </Router>
    </div>

  );
}

export default App;
