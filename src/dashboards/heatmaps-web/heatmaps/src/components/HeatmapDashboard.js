import React from 'react';
import './HeatmapDashboard.css';
import Navbar from './Navbar';
import Content from './Content';



class HeatmapDashboard extends React.Component{
    render(){
        return(
            <div className="heatmap-dashboard">
                <Navbar title="Heatmap Dashboard"/>
                <Content/>
            </div>
        )
    }

}

export default HeatmapDashboard;