import React from 'react';
import './HeatmapDashboard.css';
import Control from './Control';
import scriptLoader from 'react-async-script-loader';
import HeatMap from './HeatMap';
import PredictiveGraph from './PredictiveGraph';

import Button from 'react-bootstrap/Button';


class Content extends React.Component{

    constructor(props){
        super(props);

        this.state = {
            building: 1,
            startDate : "2019-08-01",
            startTime : "08:00",
            endDate : "2019-08-01",
            endTime : "20:00",
            predictiveToggle: true,
        };

        this.setHeatmap = this.setHeatmap.bind(this);
        this.setGraph = this.setGraph.bind(this);
    }


    updateAllFilters(building, startDate, startTime, endDate, endTime){
        this.setState((prevState,props) => ({
            building: building,
            startDate: startDate,
            startTime: startTime,
            endDate: endDate,
            endTime: endTime
        }));
    }


    setHeatmap(){
        this.setState({
            predictiveToggle: true,
        })
    }

    setGraph(){
        this.setState({
            predictiveToggle: false,
        })
    }
    

    
    render(){
        return(
            <div className="content-container">
                <div className="content-panel">
                    <div className="btn-group btn-group-toggle" data-toggle="buttons">
                        <Button onClick={this.setHeatmap} variant="outline-info">Heatmap Plot</Button>
                        <Button onClick={this.setGraph} variant="outline-info">Future Predictions</Button>
                    </div>
                </div>
                {this.state.predictiveToggle? 
                    <HeatMap 
                    id="heatmap" 
                    building={this.state.building}
                    startDate={this.state.startDate} 
                    startTime={this.state.startTime} 
                    endDate={this.state.endDate}
                    endTime={this.state.endTime}
                    />
                    :
                    <PredictiveGraph/>
                    }
            </div>
        )
    }

}
export default scriptLoader(['https://maps.googleapis.com/maps/api/js?key=AIzaSyAEtQwtf1LxZtx_qYa1bMqoGLdZd6pigMg&libraries=visualization'])(Content);
