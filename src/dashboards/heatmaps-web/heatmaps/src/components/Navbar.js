import React from 'react';
import './HeatmapDashboard.css';


function Navbar(props){
    return(
        <div>
            <nav className="navbar navbar-expand-lg bg-primary">
                <div className="nav-logo">{props.title}</div>
            </nav>
        </div>
    )
}

export default Navbar;