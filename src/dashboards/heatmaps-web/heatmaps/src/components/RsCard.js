import React from 'react';
// import './HeatmapDashboard.css';
import Navbar from './Navbar';
import Content from './Content';
import './RecSys.css';

import Rating from '@material-ui/lab/Rating';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';


import {Modal, Button as Button2} from 'react-bootstrap';




import Background from '../images/icon.png';
import Res1 from '../images/res1.png';
import Res2 from '../images/res2.png';
import Res3 from '../images/res3.png';
import Res4 from '../images/res4.png';
import Res5 from '../images/res5.png';
import Res6 from '../images/res6.png';
import Res7 from '../images/res7.png';
import Res8 from '../images/res8.png';
import Cafe from '../images/cafe.png';





class RsCard extends React.Component{

    constructor(props){
        super(props);
        var img = null;
        var imgCode = props.img;
        var restaraunts = [Res1, Res2, Res3, Res4, Res5, Res6, Res7, Res8];
        if (imgCode == "Cafe"){
            img = Cafe;
        }
        else{
            img = restaraunts[Math.floor(Math.random()*restaraunts.length)];
        }

        var reviews = [];

        if(this.props.reviews){
            this.props.reviews.forEach(function(val){
                reviews.push(<p>{val}</p>);
            });
        }
        else{
            reviews.push(<p>{"No reviews available"}</p>);
        }


        this.handleShow = this.handleShow.bind(this);
        this.handleClose = this.handleClose.bind(this);
        this.state = {
            image: img,
            show: false,
            reviews: [],
        }

    }

    handleShow(){
        console.log("Reviews");
        console.log(this.state.reviews);
        var reviews = [];

        if(this.props.reviews){
            this.props.reviews.forEach(function(val, index){
                reviews.push(                
                    <tr>
                    <th scope="row">{index+1}</th>
                    <td>{val}</td>
                    </tr>
                );
            });
        }
        else{
            reviews.push(<p>{"No reviews available"}</p>);
        }

        this.setState({
            show: true,
            reviews: reviews,
        });
    }

    handleClose(){
        this.setState({show: false});
    }
    

    componentDidUpdate(prevProps, prevState){
        if(prevProps.img!=this.props.img){
            var img = null;
            var imgCode = this.props.img;
            var restaraunts = [Res1, Res2, Res3, Res4, Res5, Res6, Res7, Res8];
            if (imgCode == "Cafe"){
                img = Cafe;
            }
            else{
                img = restaraunts[Math.floor(Math.random()*restaraunts.length)];
            }
            
            this.setState({
                image: img,
            })
        }
    }

    

    render(){

        return(
            <div className="scroll-card">
                <Modal show={this.state.show} onHide={this.handleClose}>
                <Modal.Header closeButton>
                    <Modal.Title>{this.props.title}</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <b>Rating: {this.props.value}</b><br/>
                    <b>Top Rated Reviews</b><br/>
                    <i>(Translated Portugese -> English)</i> <br/>
                    <table class="table table-bordered">
                    <thead class="thead-dark">
                        <tr>
                        <th scope="col">Review Rank</th>
                        <th scope="col">Review</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.state.reviews}

                    </tbody>
                    </table>
                </Modal.Body>
                <Modal.Footer>
                    <Button2 variant="secondary" onClick={this.handleClose}>
                        Close
                    </Button2>
                </Modal.Footer>
                </Modal>
                <div className="scroll-card-title" onClick={this.handleShow}>
                    <h5>{this.props.title}</h5>
                </div>
                <div className="scroll-card-img" style={{backgroundImage: "url(" + this.state.image + ")", backgroundSize : "100% 100%"}}>
                </div>
                <div className="scroll-card-rating">
                <Box component="fieldset" mb={3} borderColor="transparent">
                    <Typography component="legend">Rating</Typography>
                    <Rating name="disabled" value={this.props.value} precision={0.5} disabled/>
                </Box>
                </div>
            </div>

        )
    }
}



export default RsCard;