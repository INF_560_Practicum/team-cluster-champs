import React from 'react';
// import './HeatmapDashboard.css';
import Navbar from './Navbar';
import Content from './Content';
import './RecSys.css';
import RsCard from './RsCard';
import Button from '@material-ui/core/Button';




class RecSys extends React.Component{

    constructor(props){
        super(props);
        this.showModal = this.showModal.bind(this);
        this.getDataFromApiPostRio = this.getDataFromApiPostRio.bind(this);
        this.getDataFromApiPostApt = this.getDataFromApiPostApt.bind(this);
        this.cardClicked = this.cardClicked.bind(this);

        this.state = {
            loading: false,
            showModal: false,
            airportCards: [
                <RsCard key={0} title={"Loading Results"} value={5}/>,
                <RsCard key={1} title={"Loading Results"} value={5}/>,
                <RsCard key={2} title={"Loading Results"} value={5}/>,
                <RsCard key={2} title={"Loading Results"} value={5}/>
            ],
            rioCards: [
                <RsCard key={0} title={"Loading Results"} value={5}/>,
                <RsCard key={1} title={"Loading Results"} value={5}/>,
                <RsCard key={2} title={"Loading Results"} value={5}/>,
                <RsCard key={2} title={"Loading Results"} value={5}/>
            ],
        }
        this.getDataFromApiPostApt("0", "1", "0");
        this.getDataFromApiPostRio("0", "1", "0");

    }

    cardClicked(){
        console.log("GOT HERE");
        console.log(this.state);
        this.setState({
            showModal: true
        });
    }

    applyFilters(){

        var gender = document.getElementById("genderFilter").value;
        var gs = document.getElementById("groupSoloFilter").value;
        var distance = document.getElementById("distanceFilter").value;
        this.getDataFromApiPostApt(gender, gs, distance);
        this.getDataFromApiPostRio(gender, gs, distance);
    }

    getDataFromApiPostApt(gender, gs, distance){
        const google = window.google;
        var data = new FormData();
        data.gender = gender;
        data.gs = gs;
        data.distance = distance;
        console.log("DATA IS");
        console.log(data);
        var body = JSON.stringify(data);
        var clicked = this.cardClicked;
        this.setState({
          loading: true
        });
        fetch("http://localhost:3001/apt_recs/",{
          method: 'POST',
          headers: {
            'Content-Type': 'application/json'
          },
          body: body,
        })
        .then((res) => res.json())
        .then(json => {
            console.log("JSON in apt");
            console.log(json.length);
            var airportCards = []
            json.forEach(function(obj, index){
                airportCards.push(<RsCard key={index} title={obj[0]} img={obj[1]} value={obj[2]} reviews={obj[5]} />);
            });
            this.setState({
                airportCards: airportCards,
                loading: false
            })
        });
    }

    getDataFromApiPostRio(gender, gs, distance){
        const google = window.google;
        var data = new FormData();
        data.gender = gender;
        data.gs = gs;
        data.distance = distance;
        console.log("DATA IS");
        console.log(data);
        var body = JSON.stringify(data);
        var clicked = this.cardClicked;
        this.setState({
          loading: true
        });
        fetch("http://localhost:3001/rio_recs/",{
          method: 'POST',
          headers: {
            'Content-Type': 'application/json'
          },
          body: body,
        })
        .then((res) => res.json())
        .then(json => {
            console.log("JSON");
            console.log(json.length);
            var rioCards = []
            json.forEach(function(obj, index){
                rioCards.push(<RsCard key={index} title={obj[0]} img={obj[1]} value={obj[2]} reviews={obj[5]} />);
            });
            this.setState({
                rioCards: rioCards,
                loading: false
            })
        });
    }

    showModal(){
        this.setState({
            showModal: true,
        });
    }

    render(){
        return(
            <div className="">
                <Navbar title="Rio Restaraunts and Businesses"/>
                <div className="rs-container">
                    <div className="rs-sidebar">
                        <div className="card">
                            <div class="card-title"><h5>Filters</h5></div>
                            <div className="card-body">
                                <div className="filter-row">
                                    Results: {this.state.airportCards.length}
                                </div>
                                <br/>
                                <div className="filter-row">
                                    <div class="form-group">
                                        <label for="genderFilter">Gender:</label>
                                        <select class="form-control" id="genderFilter">
                                            <option value={"0"}>Both</option>
                                            <option value={"1"}>Male</option>
                                            <option value={"2"}>Female</option>
                                        </select>
                                    </div>
                                </div>
                                <div className="filter-row">
                                    <div class="form-group">
                                        <label for="groupSoloFilter">Group/Solo:</label>
                                        <select class="form-control" id="groupSoloFilter">
                                            <option value={"0"}>Both</option>
                                            <option value={"1"} selected>Solo</option>
                                            <option value={"2"}>Group</option>
                                        </select>
                                    </div>
                                </div>
                                <div className="filter-row">
                                    <div class="form-group">
                                        <label for="distanceFilter">Distance:</label>
                                        <select class="form-control" id="distanceFilter">
                                            <option value={"0"}>0-1mi</option>
                                            <option value={"5"}>1-5mi</option>
                                            <option value={"10"} selected>5-10mi</option>
                                            <option value={"20"}>10-20mi</option>
                                            <option value={"50"}>50mi+</option>
                                        </select>
                                    </div>
                                </div>
                                <Button className="filter-button" variant="contained" color="primary" onClick={this.applyFilters.bind(this)} >Update Filters</Button>

                            </div>
                        </div>
                    </div>
                    <div className="rs-content">
                        <div className="rs-row">
                            <div className="">
                                <h3>{this.state.loading? "Fetching Results": "Recommendations in Airport"}</h3>
                            </div>
                            <br/>
                            <div className="scroll-row">
                                {this.state.airportCards}
                            </div>
                        </div>

                        <div className="rs-row">
                            <div className="">
                                <h3>{this.state.loading? "Fetching Results": "Recommendations in Rio"}</h3>
                            </div>
                            <br/>
                            <div className="scroll-row">
                                {this.state.rioCards}
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        )
    }

}

export default RecSys;