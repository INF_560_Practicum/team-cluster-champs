import React, { PureComponent } from 'react';

import './HeatmapDashboard.css';
import Control from './Control';
import scriptLoader from 'react-async-script-loader';
import HeatMap from './HeatMap';
import {
    LineChart, Line, XAxis, YAxis, CartesianGrid, Tooltip, Legend,
  } from 'recharts';
  

class PredictiveGraph extends React.Component{

    render(){

        const data2 = [{"group": 27, "solo": 380, "date": "12/1/19"}, {"group": 25, "solo": 310, "date": "12/2/19"}, {"group": 24, "solo": 417, "date": "12/3/19"}, {"group": 23, "solo": 328, "date": "12/4/19"}, {"group": 23, "solo": 422, "date": "12/5/19"}, {"group": 22, "solo": 356, "date": "12/6/19"}, {"group": 22, "solo": 421, "date": "12/7/19"}, {"group": 21, "solo": 381, "date": "12/8/19"}, {"group": 21, "solo": 423, "date": "12/9/19"}, {"group": 21, "solo": 401, "date": "12/10/19"}, {"group": 20, "solo": 427, "date": "12/11/19"}, {"group": 20, "solo": 417, "date": "12/12/19"}, {"group": 20, "solo": 433, "date": "12/13/19"}, {"group": 19, "solo": 429, "date": "12/14/19"}, {"group": 19, "solo": 440, "date": "12/15/19"}, {"group": 19, "solo": 439, "date": "12/16/19"}, {"group": 18, "solo": 446, "date": "12/17/19"}, {"group": 18, "solo": 447, "date": "12/18/19"}, {"group": 18, "solo": 452, "date": "12/19/19"}, {"group": 17, "solo": 454, "date": "12/20/19"}, {"group": 17, "solo": 458, "date": "12/21/19"}, {"group": 17, "solo": 460, "date": "12/22/19"}, {"group": 16, "solo": 463, "date": "12/23/19"}, {"group": 16, "solo": 465, "date": "12/24/19"}, {"group": 16, "solo": 467, "date": "12/25/19"}, {"group": 15, "solo": 469, "date": "12/26/19"}, {"group": 15, "solo": 471, "date": "12/27/19"}, {"group": 15, "solo": 472, "date": "12/28/19"}, {"group": 15, "solo": 474, "date": "12/29/19"}, {"group": 14, "solo": 475, "date": "12/30/19"}]

        const data = [
            {
              name: 'Page A', uv: 4000, pv: 2400, amt: 2400,
            },
            {
              name: 'Page B', uv: 3000, pv: 1398, amt: 2210,
            },
            {
              name: 'Page C', uv: 2000, pv: 9800, amt: 2290,
            },
            {
              name: 'Page D', uv: 2780, pv: 3908, amt: 2000,
            },
            {
              name: 'Page E', uv: 1890, pv: 4800, amt: 2181,
            },
            {
              name: 'Page F', uv: 2390, pv: 3800, amt: 2500,
            },
            {
              name: 'Page G', uv: 3490, pv: 4300, amt: 2100,
            },
          ];

        return(
            <div className="graph-container">
                <h3>Group Traveler Forecast</h3>
                <LineChart
                    width={900}
                    height={250}
                    data={data2}
                    margin={{
                    top: 50, right: 0, left: 0, bottom: 5,
                    }}
                >
                    <CartesianGrid strokeDasharray="3 3" />
                    <XAxis dataKey="date" />
                    <YAxis />
                    <Tooltip />
                    <Legend />
                    <Line type="monotone" dataKey="group" stroke="#8884d8" activeDot={{ r: 8 }} />
                </LineChart>
                <br/> 
                <h3>Solo Traveler Forecast</h3>

                <LineChart
                    width={900}
                    height={250}
                    data={data2}
                    margin={{
                    top: 50, right: 0, left: 0, bottom: 5,
                    }}
                >
                    <CartesianGrid strokeDasharray="3 3" />
                    <XAxis dataKey="date" />
                    <YAxis />
                    <Tooltip />
                    <Legend />
                    <Line type="monotone" dataKey="solo" stroke="#82ca9d" />
                </LineChart>
            </div>
        )
    }
}

export default PredictiveGraph;