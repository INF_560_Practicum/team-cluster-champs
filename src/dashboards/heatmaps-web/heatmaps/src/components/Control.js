import React, {useState} from 'react';
import './HeatmapDashboard.css';

import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Slider from '@material-ui/core/Slider';
import TextField from '@material-ui/core/TextField';
import CssBaseline from '@material-ui/core/CssBaseline';
import Container from '@material-ui/core/Container';
import Button from '@material-ui/core/Button';
import Box from '@material-ui/core/Box';


import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';

const useStyles = makeStyles((theme) => ({
  button: {
    display: 'block',
    marginTop: theme.spacing(2),
  },
  formControl: {
    margin: theme.spacing(1),
    minWidth: 120,
  },
  container: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  textField: {
    marginLeft: theme.spacing(1),
    marginRight: theme.spacing(1),
    width: 200,
  },
}));

function BuildingSelect() {
  const classes = useStyles();
  const [building, setBuilding] = React.useState('');
  const [open, setOpen] = React.useState(false);

  const handleChange = (event) => {
    setBuilding(event.target.value);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const handleOpen = () => {
    setOpen(true);
  };

  return (
    <div>
      <Button className={classes.button} onClick={handleOpen}>
        Select Building
      </Button>
      <FormControl className={classes.formControl}>
        <InputLabel id="building-select-label">Building</InputLabel>
        <Select
          labelId="building-select-label"
          id="building-select"
          open={open}
          onClose={handleClose}
          onOpen={handleOpen}
          value={building}
          onChange={handleChange}
        >
          <MenuItem value={1}>TPS1</MenuItem>
          <MenuItem value={2}>TPS2</MenuItem>
        </Select>
      </FormControl>
    </div>
  );
}

const getDefaultDate = () =>{
    return "2019-08-01"
}

const getDefaultStartTime = ()=>{
    return "08:00"
}

const getDefaultEndTime = ()=>{
    return "20:00"
}


function getHeatmapFilters(){
  var date = new Date(document.getElementById("date").value);
  var startTime = document.getElementById("startTime").value;
  var endTime = document.getElementById("endTime").value;
  console.log("Date");
  console.log(date);
  console.log("start time");
  console.log(startTime);
  console.log("end time");
  console.log(endTime);

}

function TimePickers(props) {
  const classes = useStyles();

  return (
    <form className={classes.container} noValidate>
      <TextField
        id={props.id}
        label={props.label}
        type="time"
        defaultValue={props.defaultTime}
        className={classes.textField}
        InputLabelProps={{
          shrink: true,
        }}
        inputProps={{
          step: 300, // 5 min
        }}
        onChange={props.onChange}
      />
    </form>
  );
}

function DatePickers(props) {
  const classes = useStyles();

  return (
    <form className={classes.container} noValidate>
      <TextField
        id={props.id}
        label={props.label}
        type="date"
        defaultValue={props.defaultDate}
        className={classes.textField}
        InputLabelProps={{
          shrink: true,
        }}
        onChange={props.onChange}
      />
    </form>
  );
}








class Control extends React.Component{

    constructor(props){
        super(props);
        const defaultDate = getDefaultDate();
        const defaultStartTime = getDefaultStartTime();
        const defaultEndTime = getDefaultEndTime();

        this.state = {
            building: 1,
            buildingOpen: false,
            updateFilters: props.updateAllFilters,
            startDate: defaultDate,
            startTime: defaultStartTime,
            endDate: defaultDate,
            endTime: defaultEndTime,
        };
    }

    updateFilters(){
      var building = document.getElementById("building").value;
      var startDate = document.getElementById("startDate").value;
      var startTime = document.getElementById("startTime").value;
      var endDate = document.getElementById("endDate").value;
      var endTime = document.getElementById("endTime").value;
      this.props.applyFilters(building, startDate, startTime, endDate, endTime);
    }

    handleOpen(){
      this.setState({
        buildingOpen : true
      })
    }
    
    handleClose(){
      this.setState({
        buildingOpen : false
      })
    }

    render(props){
        return(
            <div className="container">
                <h3>Filters</h3>
                {/* <div class="form-group">
                  <label for="building">Building</label>
                  <select class="form-control" id="building">
                    <option value="1">TPS1</option>
                    <option value="2">TPS2</option>
                  </select>
                </div>
                  <DatePickers 
                id="startDate" 
                label="start date" 
                defaultDate={this.state.startDate}
                />
                <TimePickers 
                id="startTime" 
                label="start time" 
                defaultTime={this.state.startTime} 
                />
                <DatePickers 
                id="endDate" 
                label="end time"  
                defaultDate={this.state.endDate}
                />
                <TimePickers 
                id="endTime" 
                label="end time" 
                defaultTime={this.state.endTime}
                />
                <br/>
                <Button variant="contained" color="primary" onClick={this.updateFilters.bind(this)}>Apply Filters</Button> */}

            </div>
        )     
    }

}

export default Control;