import React from 'react';
import scriptLoader from 'react-async-script-loader';
import './HeatmapDashboard.css';
import Grid from '@material-ui/core/Grid';
import ToggleButton from '@material-ui/lab/ToggleButton';
import ToggleButtonGroup from '@material-ui/lab/ToggleButtonGroup';
import { makeStyles } from '@material-ui/core/styles';
import Control from './Content';
import HeatMapFilters from './HeatMapFilters';


class HeatMaps extends React.Component {

    constructor(props){
        super(props);
        this.updateAllControlFilters = this.updateAllControlFilters.bind(this);
        this.state = {
          loading: false,
          apiData: false,
          data: null,
          togglePredictive: false,
          building: 1,
          startDate : "2019-08-01",
          startTime : "08:00",
          endDate : "2019-08-01",
          endTime : "20:00",
          predictiveToggle: true,
        }
    }

    
    componentWillReceiveProps ({ isScriptLoaded, isScriptLoadSucceed }) {
        if (isScriptLoaded && !this.props.isScriptLoaded) { // load finished
          if (isScriptLoadSucceed) {
            this.getDataFromApiPost(this.state.building, this.state.startDate, this.state.startTime, this.state.endDate, this.state.endTime);
            this.setMap()
          }
          else this.props.onError()
        }
      }


    componentDidUpdate(prevProps, prevState){
      if((prevState.building != this.state.building) ||(prevState.startDate != this.state.startDate) ||(prevState.startTime != this.state.startTime) ||(prevState.endDate != this.state.endDate) || (prevState.endTime != this.state.endTime) ){
        this.getDataFromApiPost(this.state.building, this.state.startDate, this.state.startTime, this.state.endDate, this.state.endTime);
        this.setMap();
      }
    }

    getInitialData(){
        const google = window.google;

        var soloData = [
          new google.maps.LatLng(-22.812, -43.249),
        ];

        for(var i=0;i<10000;i++){
          soloData.push(new google.maps.LatLng(-22.812, -43.249));
        }

          var groupData = [
            new google.maps.LatLng(-22.812, -43.249),
          ];

          for(var i=0;i<200;i++){
            groupData.push(new google.maps.LatLng(-22.812, -43.249));
          }
  

        return [soloData, groupData];
    }


    getDataFromApiPost(building, startDate, startTime, endDate, endTime){
      const google = window.google;
      var data = new FormData();
      data.building = building;
      data.startDate = startDate;
      data.startTime = startTime;
      data.endDate = endDate;
      data.endTime = endTime;
      var gData = [];
      var sData = [];
      var body = JSON.stringify(data);
      this.setState({
        loading: true
      });
      fetch("http://localhost:3001/data/",{
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        body: body,
      })
      .then((res) => res.json())
      .then(json => {
          console.log("JSON");
          console.log(json.length);
          json.forEach((item)=>{
              var macaddr = new google.maps.LatLng(item.lat, item.lng);
              if (item.label==0){
                  sData.push(macaddr);
              }
              else{
                  gData.push(macaddr);
              }
          });
          console.log("Updating state with api data for heatmap");
          this.setState({
              loading: false,
              apiData: true,
              data: [sData, gData]
          });
          this.setMap();
      });
  }
    


    setMap(){
        const google = window.google;
        this.map = new google.maps.Map(document.getElementById('map'), {
            center: { lat: -22.812, lng: -43.249  },
            zoom: 17
          });
          
          var data = this.getInitialData();
          if (this.state.apiData){
              data = this.state.data;
          }
          console.log("DATA");
          console.log(data);

          var g1 = [
            'rgba(0, 0, 255, 0)',
            'rgba(0, 0, 255, 1)',
          ]

          var g2 = [
            'rgba(0, 255, 0, 0)',
            'rgba(0, 255, 0, 1)',
          ]

          var soloHeatMap = new google.maps.visualization.HeatmapLayer({
            data: data[0],
            gradient: g1
          });

          var groupHeatMap = new google.maps.visualization.HeatmapLayer({
            data: data[1],
            gradient: g2
          });
          
          soloHeatMap.setMap(this.map);
          groupHeatMap.setMap(this.map);
    }

    updateAllControlFilters(building, startDate, startTime, endDate, endTime){
      this.setState((prevState,props) => ({
          building: building,
          startDate: startDate,
          startTime: startTime,
          endDate: endDate,
          endTime: endTime
      }), () => {
      });
    }


  render() {
    return (
      <div className="row">
        <div className="left">
          <div className="map-header">
          {this.state.loading? "Loading ..." : "Showing Results for Days: "+this.props.startDate+" - "+this.props.endDate}<br/>
          <b>{this.state.apiData? "Solo travelers: "+this.state.data[0].length+" Group travelers: "+this.state.data[1].length : "Fetching Results"}</b>         
          </div>
          <div className="map-container">
          <div id="map" style={{height: '800px', width: '900px' }}></div>
          {!this.map && <div className="center-md">Loading...</div>}
          </div>
        </div>
        <div className="right">
            {/* <p>{this.state.building}</p>
            <p>{this.state.startDate}</p>
            <p>{this.state.startTime}</p>
            <p>{this.state.endDate}</p>
            <p>{this.state.endTime}</p> */}
            <HeatMapFilters
              applyFilters={this.updateAllControlFilters}
            />

        </div>
      </div>

    );
  }
}

export default scriptLoader(['https://maps.googleapis.com/maps/api/js?key=AIzaSyAEtQwtf1LxZtx_qYa1bMqoGLdZd6pigMg&libraries=visualization'])(HeatMaps);
