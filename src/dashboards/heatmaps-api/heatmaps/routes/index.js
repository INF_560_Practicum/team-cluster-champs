var express = require('express');
var router = express.Router();
var cors = require("cors");
var fs = require("fs");
var parse = require("csv");

var csv = require('csvtojson')
var travler_data = require('../data/rsdata');
var rio_recs = travler_data.rio_travelers;
var apt_recs = travler_data.apt_travelers;

var csvFile = "/Users/Sai/Desktop/kiana_labeled.csv"



router.get('/data', cors(), function(req, res, next){


  csv()
  .fromFile(csvFile)
  .then((json)=>{
    var new_list = json.map(function(obj) {
      var label = 0;
      var count = 0;
      if (obj.label=="group"){
        count += 1;
        label = 1;
      }
      return {
        id: obj.field1,
        datetime: obj.localtime,
        lat: obj.lat,
        lng: obj.lng,
        label: label
      }
    });
    res.json(new_list);
  });
});

router.post('/data', cors(), function(req, res, next){
  var buildingCode = req.body.building;
  var startDate = req.body.startDate;
  var endDate = req.body.endDate;
  var startTime = req.body.startTime;
  var endTime = req.body.endTime;

  console.log("POST DATA");
  console.log(req.body);
  var start = startDate+" "+startTime+":00.000 UTC";
  var end = endDate+" "+endTime+":00.000 UTC";

  var startDT = new Date(start);
  var endDT = new Date(end);

  var building = "TPS1";
  if(buildingCode != "1"){
    building = "TPS2";
  }

  csv()
  .fromFile(csvFile)
  .then((json)=>{
    var filtered = json.filter(function(obj){
      var lt = obj.localtime;
      var date = new Date(lt);

      return (startDT <= date) && (date <= endDT) && (obj.Building == building);
    });
  
    var labeled = filtered.map(function(obj){
      var label = 0;
      if (obj.label=="group"){
        label = 1;
      }
      return {
        id: obj.field1,
        datetime: obj.localtime,
        lat: obj.lat,
        lng: obj.lng,
        label: label
      }
    });
    res.json(labeled);
  });
});

router.options('/data', cors(), function(req, res, next){

  var startDate = req.body.startDate;
  var endDate = req.body.endDate;
  var startTime = req.body.start;
  var endTime = req.body.end;

  var start = startDate+" "+startTime+":00.000 UTC";
  var end = endDate+" "+endTime+":00.000 UTC";


  var startDT = new Date(start);
  var endDT = new Date(end);



  csv()
  .fromFile(csvFile)
  .then((json)=>{
    var filtered = json.filter(function(obj){
      var lt = obj.localtime;
      var date = new Date(lt);
  
      return (startDT <= date) && (date <= endDT);
    });
  
    var labeled = filtered.map(function(obj){
      var label = 0;
      if (obj.label=="group"){
        label = 1;
      }
      return {
        id: obj.field1,
        datetime: obj.localtime,
        lat: obj.lat,
        lng: obj.lng,
        label: label
      }
    });
    res.json(labeled);
  });
});

router.post('/rio_recs', cors(), function(req, res, next){

  var gender = req.body.gender;
  var gs = req.body.gs;
  var distance = req.body.distance;

  console.log("FILTERS");
  console.log(req.body);

  var gender_filtered = rio_recs.filter(function(obj){
    if(gender == "0"){
      return true;
    }
    else if (gender == "1"){
      return obj[3] == "Male";
    }
    else{
      return obj[3] == "Female";
    }
  });

  var gs_filtered = gender_filtered.filter(function(obj){
    if(gs == "0"){
      return true;
    }
    else if (gs == "1"){
      return obj[4] == "Solo";
    }
    else{
      return obj[4] == "Group";
    }
  });

  console.log(gs_filtered);

  res.json(gs_filtered);

});

router.options('/rio_recs', cors(), function(req, res, next){
  console.log("PARAMS");
  console.log(req.body);
  console.log(rio_recs);
  res.json(rio_recs);
});

router.post('/apt_recs', cors(), function(req, res, next){

  var gender = req.body.gender;
  var gs = req.body.gs;
  var distance = req.body.distance;

  console.log("FILTERS");
  console.log(req.body);

  var gender_filtered = apt_recs.filter(function(obj){
    if(gender == "0"){
      return true;
    }
    else if (gender == "1"){
      return obj[3] == "Male";
    }
    else{
      return obj[3] == "Female";
    }
  });

  var gs_filtered = gender_filtered.filter(function(obj){
    if(gs == "0"){
      return true;
    }
    else if (gs == "1"){
      return obj[4] == "Solo";
    }
    else{
      return obj[4] == "Group";
    }
  });

  console.log(gs_filtered);

  res.json(gs_filtered);

});

router.options('/apt_recs', cors(), function(req, res, next){
  console.log("PARAMS");
  console.log(req.body);
  console.log(apt_recs);
  res.json(apt_recs);
});



/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});


module.exports = router;











