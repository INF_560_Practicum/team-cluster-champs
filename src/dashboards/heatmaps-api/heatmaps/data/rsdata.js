apt_solo_female = {"BOB's": "4.7", "Hallah": "4.7", "Galeao": "4.6", "Astoria": "4.5", "Pastatore": "4.4", "Cafe": "4.2", "Bazar": "4.0", "Dartel": "4.0", "Do": "4.0", "Dufry": "4.0", "Engraxataria": "4.0", "Gatos": "4.0", "H.": "4.0", "Havaianas": "4.0", "La": "4.0", "Zeppelin": "4.0", "Santos": "3.85", "Kafe": "3.4", "O": "3.4", "Sobral,": "2.666667", "Viena": "2.4", "Banco": "2.1", "Katz": "2.0"}


apt_solo_male = {"Palheta Air Cafe": 4.8, "Casa do Pao de Queijo": 4.7, "Palheta Popular Diner": 4.4, "Chocolate Q": 4.0, "FOM": 4.0, "It Beach": 4.0, "La Selva": 4.0, "Pedacinho de Minas by Rio": 4.0, "Tamar Flower Shop": 4.0, "Zeppelin Artes": 4.0, "O Boticario": 3.4, "Santos Dumont": 2.8, "Frango Assado": 2.6, "Viena": 2.4, "Katz Chocolates": 2.0}


apt_group_female = {"Batata Inglesa": 4.9, "Palheta Air Cafe": 4.8, "Casa do Pao de Queijo": 4.7, "Hallah": 4.7, "Rei do Mate": 4.5, "Palheta Popular Diner": 4.4, "Spoleto": 4.1, "AMP Tshirt Store": 4.0, "Chilli Beans": 4.0, "Chocolate Q": 4.0, "Doarel": 4.0, "Emporio do Aco": 4.0, "Engraxataria do Presente": 4.0, "FOM": 4.0, "H. Stern": 4.0, "Linha do Horizonte": 4.0, "Pedacinho de Minas by Rio": 4.0, "Tamar Flower Shop": 4.0, "Sobral": 3.5, "Frango Assado": 2.6}


apt_male_female = {"Batata Inglesa": 4.9, "BOB's": 4.7, "Galeao Coffee Shop": 4.6, "Astoria Kilo": 4.5, "Rei do Mate": 4.5, "Cafe Aviador": 4.2, "Spoleto": 4.1, "AMP Tshirt Store": 4.0, "Bazar Solidario": 4.0, "Chilli Beans": 4.0, "Dartel Toys": 4.0, "Doarel": 4.0, "Dufry": 4.0, "It Beach": 4.0, "Linha do Horizonte": 4.0, "Sobral": 4.0, "Cafe Palheta": 3.7, "Kafe": 3.4, "O Melhor Bolo de Chocolate do Mundo": 3.2, "Banco Bradesco": 2.1}



apt_solo_travelers = [['Santos Dumont', 'Restaurant', 4.9, 'Female', 'Solo'],
 ['Palheta Air Cafe ', 'Cafe', 4.8, 'Male', 'Solo'],
 ["BOB's ", 'Cafe', 4.7, 'Female', 'Solo'],
 ['Hallah ', 'Restaurant', 4.7, 'Female', 'Solo'],
 ['Casa do Pao de Queijo ', 'Cafe', 4.7, 'Male', 'Solo'],
 ['Galeao Coffee Shop ', 'Cafe', 4.6, 'Female', 'Solo'],
 ['Astoria Kilo ', 'Cafe', 4.5, 'Female', 'Solo'],
 ['Pastatore ', 'Restaurant', 4.4, 'Male', 'Solo'],
 ['Pastatore ', 'Restaurant', 4.4, 'Female', 'Solo'],
 ['Palheta Popular Diner ', 'Restaurant', 4.4, 'Male', 'Solo']]



 apt_group_travelers = [['Sobral', 'Shop', 5.0, 'Female', 'Group'],
 ['Batata Inglesa ', 'Restaurant', 4.9, 'Female', 'Group'],
 ['Batata Inglesa ', 'Restaurant', 4.9, 'Male', 'Group'],
 ['Palheta Air Cafe ', 'Cafe', 4.8, 'Female', 'Group'],
 ['Casa do Pao de Queijo ', 'Cafe', 4.7, 'Female', 'Group'],
 ["BOB's ", 'Cafe', 4.7, 'Male', 'Group'],
 ['Hallah ', 'Restaurant', 4.7, 'Female', 'Group'],
 ['Galeao Coffee Shop ', 'Cafe', 4.6, 'Male', 'Group'],
 ['Astoria Kilo ', 'Cafe', 4.5, 'Male', 'Group'],
 ['Rei do Mate ', 'Cafe', 4.5, 'Male', 'Group']]


apt_solo_female_reviews = [
    {"BOB's":["Very good","good","Diner that has improved a lot"]},
    {"Hallah":["Not bad","good","food was ok"]},
    {"Galeao":["nice collection of toys","good shop"]},
    {"Astoria":["good","boutiqueasf were nice","cheeap and good"]},
    {"Pastatore":["It's really good!","wine collection is superb","value for money"]},
    {"Cafe":["rich food","cheap and good food","very good"]},
    {"Bazar":["appetizing batter","flavoursome","expensive but tasty"]},
    {"Dartel":["succulent food","luscious","rich and sweet"]},
    {"Do":["savoury foor","piquant meal","deliciouss"]},
    {"Dufry":["delish and good","yummy food and enjoyed it completeyly","Diner that has improved a lot"]},
    {"Engraxataria":["moreish moreish moreish","good","food was ok"]},
    {"Gatos":["nice collection of toys","peng food . Loved it very much"]},
    {"H.":["good","boutiqueasf were nice","cheeap and good"]},
    {"Havaianas":["It's really good!","inger-licking food . WILL surely visit again and again","value for money"]},
    {"La":["rich food","cheap and good food","very good"]},
    {"Zeppelin":["appetizing batter","flavoursome","expensive but tasty"]},
    {"Santos":["succulent food","luscious","rich and sweet"]},
    {"Kafe":["savoury and exquisite","piquant meal with divine . Good service","deliciouss"]},
    {"O":["substandard food","bad taste ","don't visit"]},
    {"Sobral":["low quality","cheap quality","very bad"]},
    {"Viena":["batter expensive","dont waste your money","expensive but poor quality .Dont waster money"]},
    {"Banco":["sirksome food . Annoyed after eating","Got bad stomach with the food","dont visit such places"]},
    {"Katz":["oksh food but service sucks","can't  commnet so bad . Close this shop","very poor"]}
    
]


apt_solo_male_reviews = [
{"Palheta Air Cafe":["Very good","good","Diner that has improved a lot"]},
{"Zeppelin Artes":["delish and good","yummy food and enjoyed it completeyly","Diner that has improved a lot"]},
{"O Boticario":["moreish moreish moreish","good","food was ok"]},
{"Santos Dumont":["nice collection of toys","peng food . Loved it very much"]},
{"Frango Assado":["good","boutiqueasf were nice","cheeap and good"]},
{"Casa do Pao de Queijo":["Not bad","good","food was ok"]},
{"Palheta Popular Diner":["nice collection of toys","good shop"]},
{"Chocolate Q":["good","boutiqueasf were nice","cheeap and good"]},
{"FOM":["It's really good!","wine collection is superb","value for money"]},
{"It Beach":["rich food","cheap and good food","very good"]},
{"La Selva":["appetizing batter","flavoursome","expensive but tasty"]},
{"Pedacinho de Minas by Rio":["succulent food","luscious","rich and sweet"]},
{"Tamar Flower Shop":["savoury foor","piquant meal","deliciouss"]},
{"Zeppelin Artes":["delish and good","yummy food and enjoyed it completeyly","Diner that has improved a lot"]},
{"O Boticario":["moreish moreish moreish","good","food was ok"]},
{"Santos Dumont":["nice collection of toys","peng food . Loved it very much"]},
{"Frango Assado":["good","boutiqueasf were nice","cheeap and good"]},
{"Viena":["It's really good!","inger-licking food . WILL surely visit again and again","value for money"]},
{"Katz Chocolates":["rich food","cheap and good food","very good"]}
]





rio_solo_female = {"Delirio Tropical - Rio Galeao": "4.7", "TGI Friday's": "4.7", "Churrascaria Mocellin": "4.6", "Rei do Mate": "4.5", "Tragga Humaita": "4.4", "Piola": "4.2", "Havanna Cafe": "4.0", "Restaurante Siri da Ilha": "4.0", "Plaza Premium Lounge": "4.0", "Pobre Juan": "4.0", "Restaurante Dirigivel": "4.0", "Hallah Arabian": "4.0", "McDonald's": "4.0", "Divino Fogao": "4.0", "Cafe Palheta": "4.0", "Demoiselle": "4.0", "Wine Flight": "3.85", "Kafe": "3.4", "O": "3.4", "Lanchonete Dom Galeon,": "2.3.3"}
rio_solo_male = {"Restaurante Dirigivel": 4.8, "Hallah Arabian": 4.7, "PMcDonald's": 4.4, "Divino Fogao": 4.0, "FOM": 4.0, "afe Palheta": 4.0, "Piola": 4.0, "Churrascaria Mocellin": 4.0, "Delirio Tropical - Rio Galeao": 4.0, "ZCafe Suplicy": 4.0, "O Boticario": 3.4, "STragga Vogue Square": 2.8, "Rei do Mate": 2.6, "Xeinha": 2.4, "Mocellin Pizzaria": 2.0}
rio_group_female = {"Marius Degustare": 4.9, "Tres Marias": 4.8, "Souza Irmaos Grill": 4.7, "Athelie Do Lanche": 4.7, "Restaurante Vamo": 4.5, "DeltaExpresso": 4.4, "BOTEQUIM RIO ANTIGO": 4.1, "Restaurante Bem Bolado": 4.0, "Sushilandia": 4.0, "Pobre Juan": 4.0, "Dalmu'S": 4.0, "Torre Das Pizzas": 4.0, "Strada Pizza & Grill": 4.0, "Retalhos Bar E Restaurante": 4.0, "Delicias Do Brasil Croissant E Cia": 4.0, "Restaurante Da Jane": 4.0, "Churrascaria Majorica": 4.0, "Padaria Praca Do Aviao": 4.0, "Tacho de Barro": 3.5, "FCB Burguer's & Cia": 2.6}
rio_group_male = {"Xeinha": 4.9, "TGI Friday's": 4.8, "Souza Irmaos Grill": 4.7, "ATragga Humaita": 4.7, "Havanna Cafe": 4.5, "DeltaExpresso": 4.4, "BOTEQUIM RIO ANTIGO": 4.1, "Rei do Mate": 4.0, "Sushilandia": 4.0, "elirio Tropical - Rio Galeao": 4.0, "Wine Flight": 4.0, "Tibo": 4.0, "Spazziano": 4.0, "RRota RJ 65": 4.0, "Point Das 5 Bocas": 4.0, " Suburbios Bar": 4.0, "Churrascaria Majorica": 4.0}

rio_group_travelers = [
    ['Delirio Tropical - Rio Galeao', 'Restaurant', 5.0, 'Female', 'Group'],
    ['TGI Friday\'s', 'Restaurant', 4.9, 'Female', 'Group'],
    ['Churrascaria Mocellin ', 'Restaurant', 4.9, 'Male', 'Group'],
    ['Piola', 'Cafe', 4.8, 'Female', 'Group'],
    ['Havanna Cafe', 'Cafe', 4.7, 'Female', 'Group'],
    ["Restaurante Siri da Ilha", 'Cafe', 4.7, 'Male', 'Group'],
    ['Tragga Vogue Square ', 'Restaurant', 4.7, 'Female', 'Group'],
    ['Galeao Coffee Shop ', 'Cafe', 4.6, 'Male', 'Group'],
    ['Restaurante Dirigivel', 'Cafe', 4.5, 'Male', 'Group'],
    ['Divino Fogao', 'Cafe', 4.5, 'Male', 'Group']
]

rio_solo_travelers =  [
    ['Marius Degustare', 'Restaurant', 4.9, 'Female', 'Solo'],
    ['Palheta Air Cafe ', 'Cafe', 4.8, 'Male', 'Solo'],
    ["BOB's ", 'Cafe', 4.7, 'Female', 'Solo'],
    ['hurrascaria Mocellin', 'Restaurant', 4.7, 'Female', 'Solo'],
    ['Casa do Pao de Queijo ', 'Cafe', 4.7, 'Male', 'Solo'],
    ['rOTEQUIM RIO ANTIGO', 'Cafe', 4.6, 'Female', 'Solo'],
    ['Spazziano', 'Cafe', 4.5, 'Female', 'Solo'],
    ['Sushilandia ', 'Restaurant', 4.4, 'Male', 'Solo'],
    ['McDonald\'s ', 'Restaurant', 4.4, 'Female', 'Solo'],
    ['Wine Flight', 'Restaurant', 4.4, 'Male', 'Solo']
]

apt_all_travelers = [
    ['Santos Dumont', 'Restaurant', 4.9, 'Female', 'Solo', ["succulent food","luscious","rich and sweet"]],
    ['Palheta Air Cafe ', 'Cafe', 4.8, 'Male', 'Solo', ["Very good","good","Diner that has improved a lot"]],
    ["BOB's ", 'Cafe', 4.7, 'Female', 'Solo', ["Very good","good","Diner that has improved a lot"]],
    ['Hallah ', 'Restaurant', 4.7, 'Female', 'Solo', ["Not bad","good","food was ok"]],
    ['Casa do Pao de Queijo ', 'Cafe', 4.7, 'Male', 'Solo', ["Not bad","good","food was ok"]],
    ['Galeao Coffee Shop ', 'Cafe', 4.6, 'Female', 'Solo', ["nice collection of toys","good shop"]],
    ['Astoria Kilo ', 'Cafe', 4.5, 'Female', 'Solo', ["good","boutiqueasf were nice","cheeap and good"]],
    ['Pastatore ', 'Restaurant', 4.4, 'Male', 'Solo', []],
    ['Pastatore ', 'Restaurant', 4.4, 'Female', 'Solo', ["It's really good!","wine collection is superb","value for money"]],
    ['Palheta Popular Diner ', 'Restaurant', 4.4, 'Male', 'Solo', ["nice collection of toys","good shop"]],
    ['Sobral', 'Shop', 5.0, 'Female', 'Group', ["low quality","cheap quality","very bad"]],
    ['Batata Inglesa ', 'Restaurant', 4.9, 'Female', 'Group', []],
    ['Batata Inglesa ', 'Restaurant', 4.9, 'Male', 'Group', []],
    ['Palheta Air Cafe ', 'Cafe', 4.8, 'Female', 'Group', ["rich food","cheap and good food","very good"]],
    ['Casa do Pao de Queijo ', 'Cafe', 4.7, 'Female', 'Group', ["Not bad","good","food was ok"]],
    ["BOB's ", 'Cafe', 4.7, 'Male', 'Group', []],
    ['Hallah ', 'Restaurant', 4.7, 'Female', 'Group', ["Not bad","good","food was ok"]],
    ['Galeao Coffee Shop ', 'Cafe', 4.6, 'Male', 'Group', []],
    ['Astoria Kilo ', 'Cafe', 4.5, 'Male', 'Group', []],
    ['Rei do Mate ', 'Cafe', 4.5, 'Male', 'Group', []]
]


rio_all_travelers = [
    ['Delirio Tropical - Rio Galeao', 'Restaurant', 5.0, 'Female', 'Group'],
    ['TGI Friday\'s', 'Restaurant', 4.9, 'Female', 'Group'],
    ['Churrascaria Mocellin ', 'Restaurant', 4.9, 'Male', 'Group'],
    ['Piola', 'Cafe', 4.8, 'Female', 'Group'],
    ['Havanna Cafe', 'Cafe', 4.7, 'Female', 'Group'],
    ["Restaurante Siri da Ilha", 'Cafe', 4.7, 'Male', 'Group'],
    ['Tragga Vogue Square ', 'Restaurant', 4.7, 'Female', 'Group'],
    ['Galeao Coffee Shop ', 'Cafe', 4.6, 'Male', 'Group'],
    ['Restaurante Dirigivel', 'Cafe', 4.5, 'Male', 'Group'],
    ['Divino Fogao', 'Cafe', 4.5, 'Male', 'Group'], 
    ['Marius Degustare', 'Restaurant', 4.9, 'Female', 'Solo'],
    ['Palheta Air Cafe ', 'Cafe', 4.8, 'Male', 'Solo'],
    ["BOB's ", 'Cafe', 4.7, 'Female', 'Solo'],
    ['hurrascaria Mocellin', 'Restaurant', 4.7, 'Female', 'Solo'],
    ['Casa do Pao de Queijo ', 'Cafe', 4.7, 'Male', 'Solo'],
    ['rOTEQUIM RIO ANTIGO', 'Cafe', 4.6, 'Female', 'Solo'],
    ['Spazziano', 'Cafe', 4.5, 'Female', 'Solo'],
    ['Sushilandia ', 'Restaurant', 4.4, 'Male', 'Solo'],
    ['McDonald\'s ', 'Restaurant', 4.4, 'Female', 'Solo'],
    ['Wine Flight', 'Restaurant', 4.4, 'Male', 'Solo'] 
]

apt_all_travelers = [
    ['Santos Dumont', 'Restaurant', 4.9, 'Female', 'Solo', ["succulent food","luscious","rich and sweet"]],
    ['Palheta Air Cafe ', 'Cafe', 4.8, 'Male', 'Solo', ["Very good","good","Diner that has improved a lot"]],
    ["BOB's ", 'Cafe', 4.7, 'Female', 'Solo', ["Very good","good","Diner that has improved a lot"]],
    ['Hallah ', 'Restaurant', 4.7, 'Female', 'Solo', ["Not bad","good","food was ok"]],
    ['Casa do Pao de Queijo ', 'Cafe', 4.7, 'Male', 'Solo', ["Not bad","good","food was ok"]],
    ['Galeao Coffee Shop ', 'Cafe', 4.6, 'Female', 'Solo', ["nice collection of toys","good shop"]],
    ['Astoria Kilo ', 'Cafe', 4.5, 'Female', 'Solo', ["good","boutiqueasf were nice","cheeap and good"]],
    ['Pastatore ', 'Restaurant', 4.4, 'Male', 'Solo', ["It's really good!","wine collection is superb","value for money"]],
    ['Pastatore ', 'Restaurant', 4.4, 'Female', 'Solo', ["It's really good!","wine collection is superb","value for money"]],
    ['Palheta Popular Diner ', 'Restaurant', 4.4, 'Male', 'Solo', ["nice collection of toys","good shop"]],
    ['Sobral', 'Shop', 5.0, 'Female', 'Group', ["low quality","cheap quality","very bad"]],
    ['Batata Inglesa ', 'Restaurant', 4.9, 'Female', 'Group', ["good food with fine quality","must visit . Best in neighbourhood","Just loved it"]],
    ['Batata Inglesa ', 'Restaurant', 4.9, 'Male', 'Group', ["good food with fine quality","must visit . Best in neighbourhood","Just loved it"]],
    ['Palheta Air Cafe ', 'Cafe', 4.8, 'Female', 'Group', ["rich food","cheap and good food","very good"]],
    ['Casa do Pao de Queijo ', 'Cafe', 4.7, 'Female', 'Group', ["Not bad","good","food was ok"]],
    ["BOB's ", 'Cafe', 4.7, 'Male', 'Group', ["moreish moreish moreish","good","food was ok"]],
    ['Hallah ', 'Restaurant', 4.7, 'Female', 'Group', ["Not bad","good","food was ok"]],
    ['Galeao Coffee Shop ', 'Cafe', 4.6, 'Male', 'Group', ["Just loved the coffee","Cheap coffee with quanity too much"]],
    ['Astoria Kilo ', 'Cafe', 4.5, 'Male', 'Group', ["Great cake","Celebrity place","A bit expensive"]],
    ['Rei do Mate ', 'Cafe', 4.5, 'Male', 'Group', ["appetizing batter","tasty and good","expensive but tasty"]]
]


rio_all_travelers = [
    ['Delirio Tropical - Rio Galeao', 'Restaurant', 5.0, 'Female', 'Group',["Ideal for Aeeroporto","Service can be better.","Good cost-benefit"]],
    ['TGI Friday\'s', 'Restaurant', 4.9, 'Female', 'Group',["Great option for an airport","Good option in the galleon. Some","Comforting meals! I asked"]],
    ['Churrascaria Mocellin ', 'Restaurant', 4.9, 'Male', 'Group',["Fantastic meats","Very good meat on the way to the airport","Wonderful, tender meats, wonderful cuts!"]],
    ['Piola', 'Cafe', 4.8, 'Female', 'Group',["good","boutiqueasf were nice","cheeap and good"]],
    ['Havanna Cafe', 'Cafe', 4.7, 'Female', 'Group',["Good option before boarding","wine collection is superb","It is not essential! Visit is not necessary!"]],
    ["Restaurante Siri da Ilha", 'Cafe', 4.7, 'Male', 'Group',["The risotto is awesome","Wonderful Restaurant","The best shrimp risotto"]],
    ['Tragga Vogue Square ', 'Restaurant', 4.7, 'Female', 'Group',["MORE THAN WE EXPECTED","great meat","HAPPY HOUR NO TRAGGA 🍹"]],
    ['Galeao Coffee Shop ', 'Cafe', 4.6, 'Male', 'Group',["succulent food","luscious","rich and sweet"]],
    ['Restaurante Dirigivel', 'Cafe', 4.5, 'Male', 'Group',["great buffet, with good service","Highlight for the picanha","Adjacent to the Hotel"]],
    ['Divino Fogao', 'Cafe', 4.5, 'Male', 'Group',["delish and good","yummy food and enjoyed it completeyly","Diner that has improved a lot"]],
    ['Marius Degustare', 'Restaurant', 4.9, 'Female', 'Solo',["Unbelievable","I decided to have lunch here","Best restaurant of life !!!"]],
    ['Palheta Air Cafe ', 'Cafe', 4.8, 'Male', 'Solo',["TRADITIONAL CAFETERIA","yummy food and enjoyed it completeyly","Diner that has improved a lot"]],
    ["BOB's ", 'Cafe', 4.7, 'Female', 'Solo',["moreish moreish moreish","good","food was ok"]],
    ['hurrascaria Mocellin', 'Restaurant', 4.7, 'Female', 'Solo',["nice collection of toys","peng food . Loved it very much"]],
    ['Casa do Pao de Queijo ', 'Cafe', 4.7, 'Male', 'Solo',["Already know what to expect","nice to meet","Good cheese bread, terrible value for money"]],
    ['rOTEQUIM RIO ANTIGO', 'Cafe', 4.6, 'Female', 'Solo',["nice collection of toys","good shop"]],
    ['Spazziano', 'Cafe', 4.5, 'Female', 'Solo',["Free buffet. Simple location. Great food","I recommend!","Hidden gem in Ipanema"]],
    ['Sushilandia ', 'Restaurant', 4.4, 'Male', 'Solo',["It's really good!","Very good","I recommend!!"]],
    ['McDonald\'s ', 'Restaurant', 4.4, 'Female', 'Solo',["rich food","cheap and good food","very good"]],
    ['Wine Flight', 'Restaurant', 4.4, 'Male', 'Solo',["appetizing batter","flavoursome","expensive but tasty"]]
]

module.exports.apt_travelers = apt_all_travelers;
module.exports.rio_travelers = rio_all_travelers;

